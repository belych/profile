import React, { Component } from "react";
import '../styles/index.css';
import Header from './Header';
import Skills from './Skills';

class App extends Component {
    render() {
        return (
                <div className='container'>
                    <Header/>
                    <Skills/>
                </div>
        );
    }
}

export default App;
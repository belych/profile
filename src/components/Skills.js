import React, { Component } from "react";
import '../styles/index.css';

class Skills extends Component{
    render(){
        const skills = ['JS','HTML5','CSS3','SASS','БЭМ','Git','React(в процессе изучения)']
        return(
            <div className='skills'>
                <ul className='skills__list'>
                    {
                        skills.map((item, i)=>{
                            return <li key={i} className={(i%2 ? 'skills__item color-one' : 'skills__item color-two')}>{item}</li>
                        })
                    }
                </ul>
            </div>
        )
    }
}

export default Skills;
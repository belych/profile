import React, { Component } from 'react';
import Modal from 'react-modal';
import '../styles/index.css';

const customStyles = {
    content : {
      top                   : '50%',
      left                  : '50%',
      right                 : 'auto',
      bottom                : 'auto',
      transform             : 'translate(-50%, -50%)'
    }
  };
   
  Modal.setAppElement('#root')

class Header extends Component{
    constructor() {
        super();
     
        this.state = {
          modalIsOpen: false
        };
     
        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
      }
     
      openModal() {
        this.setState({modalIsOpen: true});
      }
     
      afterOpenModal() {
        this.subtitle.style.color = '#f00';
        this.subtitle.style.height = '550px';
        this.subtitle.style.marginTop = '5px';
      }
     
      closeModal() {
        this.setState({modalIsOpen: false});
      }
    render(){
        return(
            <div className='parent'>
                <div className='parent__header'> 
                    <div className='header'>
                        <div onClick={this.openModal} className='container-img'>
                            <img className='img' src='https://pp.userapi.com/c623126/v623126693/2c1d/7LyuNIT79oQ.jpg' alt='my-photo'/>
                        </div>
                        <div className='header__text'>Хотелось бы развиваться в программировании. Привлекают в этой работе интересные задачи, возможность все время изучать что-то новое. В данный момент изучаю React.</div>
                    </div>
                </div>
                <Modal
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal}
          onRequestClose={this.closeModal}
          style={customStyles}
        >
            <button className='button' onClick={this.closeModal}>X</button>
            <div>           
                 <img ref={subtitle => this.subtitle = subtitle} src='https://pp.userapi.com/c623126/v623126693/2c1d/7LyuNIT79oQ.jpg' alt='my-photo'/>
            </div>
        </Modal>
            </div>
        )
    }
}

export default Header;